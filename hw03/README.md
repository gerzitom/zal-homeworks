# Kalkulačka
Vytvořte soubor calculator.py, v kterém naimplementujte následující funkce/operace (v závorce název funkce a počet parametrů, která funkce potřebuje):

1. Sčítání (`addition` 2)
2. Odčítání (`subtraction` 2)
3. Násobení (`multiplication` 2)
4. Dělení (`division` 2)
5. Výpočet modula (`modulo` 2, v našem případě mohou být použita pouze varianty při kterých platí `x >= y` a `y > 0`)
6. Výpočet druhé mocniny čísly (`secondPower` 1)
7. Výpočet n-té mocniny čísla (`power` 2, pořadí: mocněnec, mocnitel, v našem případě mohou být použiti pouze kladní mocnitelé (nula je kladná)) - vracejte typ `float`.
8. Výpočet druhé odmocniny z čísla (`secondRadix` 1, odmocninu lze provést pouze z kladného celého čísla splňující: x > 0)
9. Magický výpočet (`magic` 4) - operace bude vysvětlena
10. Hlavní ovládací funkce (`control` 5) - funkce bude vysvětlena

Každá z výše uvedených funkcí vrací na výstupu výpočet. V případě, že výpočet nelze provést tak funkce vyhodí výjímku `ValueError('This operation is not supported for given input parameters')`.

## Magický výpočet
Na vstupu přijímá 4 parametry x, y, z, k. Funkce nejprve spočítá x + k = l, y + z = m a pote vypocte ((l / m) + 1) = z. Hodnotu v proměnné z vrátí na výstupu.

## Hlavní ovládací funkce
Na vstupu přijímá 5 parametrů a,x,y,z,k, na výstupu je výpočet konkrétní operace. Proměnná a určuje, která z operací se má zavolat. Funkce vždy obdrží v proměnné a textovou hodnotu na jejím základě se rozhodne co za funkci zavolat. Mapování je následující:

- ADDITION = funkce číslo 1 (addition)
- SUBTRACTION = funkce číslo 2 (subtraction )
- MULTIPLICATION = funkce číslo 3 (multiplication )
- DIVISION = funkce číslo 4 (division )
- MOD = funkce číslo 5 (modulo)
- POWER = funkce číslo 7 (power)
- SECONDRADIX = funkce číslo 8 (second radix)
- MAGIC = funkce číslo 9 (magic)

Parametry x,y,z,k se poté předají konkrétní funkci v pořadí v jakém byly obdrženy. Pokud funkce vyžaduje pouze jeden parametr předá se jí proměnná x, pokud dva parametry předají se jí x,y pokud čtyři parametry předají se ji x,y,z,k. Příklad volání:

control('ADDITION', 2,3,4,5) zavolá funkci addition(2,3), která udělá 2+3 = 5. Funkce control tedy vrátí hodnotu 5.

control('SUBTRACTION', 2,3,4,5) zavolá funkci subtraction (2,3), která udělá 2-3 = -1. Funkce control tedy vrátí hodnotu -1.

control('DIVISION', 1,0,4,5) zavolá funkci division (1,0), která udělá 1/0 = nulou nelze dělit. Funkce tedy vyhodí vyjímku dle zadání: ValueError('This operation is not supported for given input parameters')

Funkce control podporuje pouze funkce vypsané v mapování - pokud by chtěl uživatel volat jakoukoliv jinou funkce tak systém vyhodí opět vyjímku: ValueError('This operation is not supported for given input parameters')