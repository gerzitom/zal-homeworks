# Showroom
Vaším úkolem je vytvořit jednoduchou databázi (dále jen db) aut. K implementaci použijte spojový seznam. Databáze aut má metodu na její vytvoření z předem definovaného pole, metodu na vložení automobilu do db, metodu na úpravu názvu již vloženého automobilu, metodu na úpravu značky již vloženého automobilu a metodu na změny stavu již vloženého automobilu. Pro zjednodušení není umožňěno měnit cenu automobilu. Dále databáze disponuje metodou, která umí vypočítat celkovou hodnotu aut v showroomu. Databáze má také metodu, která vrátí hlavičku spojového seznamu, spojový seznam samotný a metodu, která databázi vymaže. Více informací o metodách níže.

Pro databázi platí následující omezení: V db nejsou žádná ID vícekrát. V db jsou záznamy vždy seřazeny podle ceny od nejnižší po nejvyšší a to v pořadí v jakém byly vkládány.

Pro spojový seznam platí následující omezení: Každý uzel vidí na svého předka a na svého následníka. V každém uzlu jsou uložena všechna data o konkrétním automobilu.


## Datová struktura

Vytvořte třídu showroom.py, která bude obsahovat níže specifikované třídy a metody.

### Car

Třída `Car` má následující atributy: `identification`, `name`, `brand`, `price`, `active`:

`identification` - jednoznačný identifikátor vozu (`int`)
`name` - název vozu (`string`)
`brand` - značka (`string`)
`price` - cena (`int`)
`active` - příznak, zdali je vůz aktivní (`boolean`)
Instance třídy `Car` představují data, jež jsou uložena v každém uzlu (tj. `Node`).

### Node

Třída `Node` reprezentuje uzel ve spojovém seznamu. V našem případě má následující proměnné: `nextNode`, `prevNode`, `data`. Níže je popis jednotlivých proměnných:

- `nextNode` - další uzel (typu `Node`) v cestě, v případě že žádný další uzel neexistuje, pak je hodnota `None`.
- `prevNode` - předchozí uzel (typu `Node`) v cestě, v případě že předchozí uzel neexistuje, pak je hodnota `None`.
- `data` - data, která jsou uložena v aktuálním uzlu (v našem případě typu `Car`) Každý uzel musí obsahovat data.

V případě posledního uzlu ve spojovém seznamu je hodnota `nextNode = None`, v případě prvního uzlu ve spojovém seznamu je hodnota `prevNode = None`, v případě jednoprvkového seznamu je v proměných hlavy (`head`) spojového seznamu hodnota `nextNode = prevNode = None`. V případě prázdného spojového seznamu je hodnota hlavy `None`.

### LinkedList

Třída `LinkedList` reprezentuje spojový seznam a drží si hlavičku (`head`) spojového seznamu, pomocí které lze projít celý spojový seznam, to jest prohledat db. Tato třída může obsahovat i metody, které pracují se spojovým seznamem. Spojový seznam tedy implementujete jako třídu s vlastnimi atributy, ale metody pro obsluhu db můžete rovnou implementovat jako prosté funkce v souboru `showroom.py`.

## Metody

Níže je seznam funkcí, které musí databáze poskytovat (očekáváme je v souboru showroom.py), včetně jejich definice a popisu.

- `init(cars)` - metoda příjímá pole objektů typu Car a na jejich základě vytvoří spojový seznam.
- `getDatabase()` - metoda vrátí celý spojový seznam (LinkedList).
- `getDatabaseHead()` - metoda vrátí hlavičku spojového seznamu (Node).
- `clean()` - metoda vyčistí spojový seznam.
- `add(car)` - metoda na vstupu přijímá objekt typu Car a vloží ho na správné místo ve spojovém seznamu.
- `updateName(identification, name)` - metoda na vstupu přijímá identifikátor vozu (identification typu int) a nové jméno vozu (name typu string). Ve spojovém seznamu najde auto se daným indetifikátorem a nahradí u něj jméno.
- `updateBrand(identification, brand)` - metoda na vstupu přijímá identifikátor vozu a novou značku vozu (brand typu string). Ve spojovém seznamu najde auto se daným indentifikátorem a nahradí u něj značku.
- `activateCar(identification)` - metoda na vstupu přijímá identifikátor vozu. Ve spojovém seznamu, najde auto se stejným identifikátorem a nastaví jeho atribut active = True.
- `deactivateCar(identification)` - metoda na vstupu přijímá identifikátor vozu a ve spojovém seznamu, najde auto se stejným identifikátorem a nastaví jeho atribut active = False.
- `calculateCarPrice()` - metoda vrátí aktuální cenu všech vozů ve spojovém seznamu. Cena všech vozů se počítá jako součet cen aktivních vozů (tj. active == True).

> V případě, že se metodám `updateName()`, `updateBrand()`, `activateCar()`, `deactivateCar()` nepodaří najít vůz podle daného identifikátoru, metody vrátí `None`.

## Nutné podmínky

V seznamu mohou být pouze auta s unikátním ID. Unikátnost tedy není nutné ověřovat.
V rámci vaší implementace musíte zajistit aby auta v seznamu byly vždy seřazené podle ceny (vzestupně).
Při metodě `init()` a `add()` musí implementace zajistit, aby v případě kdy dva vozy mají stejnou pořizovací cenu, byly ve spojovém seznamu v pořadí v jakém byly vkládány.
V seznamu nesmí existovat záznám, který nemá data.