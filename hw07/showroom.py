class Node:
    def __init__(self, nextNode, prevNode, data):
        self.nextNode = nextNode
        self.prevNode = prevNode
        self.data = data


class LinkedList:
    def __init__(self):
        self.head = None


class Car:
    def __init__(self, identification, name, brand, price, active):
        self.identification = identification
        self.name = name
        self.brand = brand
        self.price = price
        self.active = active

db = LinkedList()


def init(cars):
    for car in cars:
        add(car)


def add(car):
    if db.head is None:
        db.head = Node(None, None, car)
    else:
        #find position
        if db.head.data.price > car.price:
            #inserted price is lower than head, insert before head
            new_node = Node(db.head, None, car)
            db.head.prevNode = new_node
            db.head = new_node
        else:
            cur_node = db.head
            while cur_node is not None:
                if cur_node.data.price < car.price:
                    if cur_node.nextNode is None:
                        break
                    else:
                        if cur_node.nextNode.data.price > car.price:
                            break
                if cur_node.nextNode is None:
                    break
                cur_node = cur_node.nextNode

            # insert after cur_node
            new_node = Node(None, cur_node, car)
            if cur_node.nextNode is not None:
                new_node.nextNode = cur_node.nextNode
                cur_node.nextNode.prevNode = new_node

            cur_node.nextNode = new_node


def updateName(identification, name):
    cur_node = db.head
    name_found = False
    while cur_node is not None:
        if cur_node.data.identification is identification:
            cur_node.data.name = name
            name_found = True
        cur_node = cur_node.nextNode

    return name_found



def updateBrand(identification, brand):
    cur_node = db.head
    while cur_node is not None:
        if cur_node.data.identification is identification:
            cur_node.data.brand = brand
        cur_node = cur_node.nextNode


def activateCar(identification):
    cur_node = db.head
    while cur_node is not None:
        if cur_node.data.identification is identification:
            cur_node.data.active = True
        cur_node = cur_node.nextNode


def deactivateCar(identification):
    cur_node = db.head
    while cur_node is not None:
        if cur_node.data.identification is identification:
            cur_node.data.active = False
        cur_node = cur_node.nextNode


def getDatabaseHead():
    return db.head


def getDatabase():
    return db


def calculateCarPrice():
    total_price = 0
    cur_node = db.head
    while cur_node is not None:
        if cur_node.data.active is True:
            total_price += cur_node.data.price
        cur_node = cur_node.nextNode
    return total_price


def clean():
    temp = db.head
    if temp is not None:
        db.head = None

# my functions
def printListNames():
    cur_node = db.head
    while cur_node is not None:
        print(cur_node.data.name, cur_node.data.price)
        cur_node = cur_node.nextNode

def getCar(carID):
    cur_node = db.head
    ret = None
    if cur_node is None:
        return False
    while cur_node is not None:
        if cur_node.data.identification == carID:
            ret = cur_node.data
        cur_node = cur_node.nextNode

    return ret


