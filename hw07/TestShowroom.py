import unittest
import showroom

class TestShowroom(unittest.TestCase):
    def testInit(self):
        showroom.init([
            showroom.Car(1, "Octavia", "Skoda", 123000, True),
            showroom.Car(23, "Felicia", "Skoda", 5000, True),
            showroom.Car(11, "Superb", "Skoda", 54000, True)
        ])

    def testAdd(self):
        showroom.add(showroom.Car(20, "Octavia", "Skoda", 2000, True))

    # def testPrintList(self):
    #     showroom.printListNames()

    def testUpdateName(self):
        showroom.updateName(23, "Model 3")
        self.assertEqual(showroom.getCar(23).name, "Model 3")

    def testUpdateBrand(self):
        showroom.updateBrand(23, "Tesla")
        self.assertEqual(showroom.getCar(23).brand, "Tesla")

    def testDeactivateCar(self):
        showroom.init([
            showroom.Car(1, "Octavia", "Skoda", 123000, True),
            showroom.Car(23, "Felicia", "Skoda", 5000, True),
            showroom.Car(11, "Superb", "Skoda", 54000, True)
        ])
        showroom.deactivateCar(23)
        self.assertEqual(showroom.getCar(23).active, False)

    def testClean(self):
        showroom.clean()

    def testActivateCar(self):
        showroom.init([
            showroom.Car(1, "Octavia", "Skoda", 123000, True),
            showroom.Car(23, "Felicia", "Skoda", 5000, True),
            showroom.Car(11, "Superb", "Skoda", 54000, True)
        ])
        showroom.activateCar(23)
        self.assertEqual(showroom.getCar(23).active, True)

    def testPriceCalculation(self):
        showroom.printListNames()
        # self.assertEqual(showroom.calculateCarPrice(), 30)

if __name__ == "__main__":
    unittest.main()
