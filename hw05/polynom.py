def polyEval(poly, x):
    value = 0
    for i in range(len(poly) - 1, -1, -1):
        if i == len(poly) - 1:
            value = poly[i]
        else:
            value = (value * x) + poly[i]
    return value

def polySum(poly1, poly2):

    polyLen = max(len(poly1), len(poly2))
    newPoly = []
    poly1Len = len(poly1)
    poly2Len = len(poly2)

    for i in range(polyLen):
        newPoly.append(0)
        if poly1Len - 1 >= i:
            newPoly[i] += poly1[i]
        if poly2Len - 1 >= i:
            newPoly[i] += poly2[i]

    for i in range(len(newPoly) - 1, -1, -1):
        if(newPoly[i] == 0):
            newPoly.pop()
        else:
            break;

    return newPoly


def polyMultiply(poly1, poly2):
    newPoly = [0] * (len(poly1) + len(poly2) - 1)
    for i in range(len(poly1)):
        for y in range(len(poly2)):
            newPoly[i + y] += poly1[i] * poly2[y]
    return newPoly

