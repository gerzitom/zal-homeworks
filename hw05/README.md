# Polynomy
Úkolem je vytvořit soubor polynom.py, v kterém jsou tři funkce pro základní operace s polynomy. Konkrétně se jedná o výpočet hodnoty polynomu, součet dvou polynomů a součin dvou polynomů. Polynomy budeme reprezentovat v Pythonu pomocí datového typu list. Položkami listů budou čísla datového typu float odpovídající koeficientům daného polynomu. Definujme, že první položka listu s indexem nula bude odpovídat koeficientu u konstantního členu, druhá koeficientu u lineárního členu, třetí koeficientu u kvadratického členu, atd…

Tudíž například list `poly1 = [1, 2.5, 3.5, 0, 5.4]` reprezentuje polynom:


Aby byla reprezentace jednoznačná, tak budeme vždy uvádět pouze nejmenší možný počet koeficientů. Neboli nulové koeficienty u nejvyšších členů budeme zahazovat. Reprezentace `poly1 = [1, 2.5, 3.5, 0, 5.4, 0]` by tedy byla považována za špatnou.

## Výpočet hodnoty polynomu


Výpočet hodnoty polynomu

Vytvořte funkci `polyEval` s dvěma parametry (polynom, x), která bude vracet hodnotu polynomu v bodě x.

To znamená, že volání `polyEval(poly1, 0)` vrátí 1, volání `polyEval(poly1, 2)` vrátí 106.4, apod.


>Zamyslete nad efektivností vašeho algoritmu a zkuste se inspirovat Hornerovým schématem.

## Součet dvou polynomů

Vytvořte funkci polySum s dvěma parametry (poly1, poly2), jež bude vracet nový polynom odpovídající součtu zadaných polynomů.

Například: volání `polySum([1, 2, 5], [2, 0, 1, -7])` vrátí `[3, 2, 6, -7]`.

## Součin dvou polynomů

Vytvořte funkci polyMultiply s dvěma parametry (poly1, poly2), jež bude vracet nový polynom odpovídající součinu zadaných polynomů.

Například: volání `polyMultiply([1, 2, 5], [2, 0, 1, -7])` vrátí `[2, 4, 11, -5, -9, -35]`.