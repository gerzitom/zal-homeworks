import unittest
import polynom

class TestPolynoms(unittest.TestCase):
    def testSum(self):
        poly1 = [1, 2.5, 3.5, 0, 5.4]
        poly2 = [-1, -3.5, -3.5, 0, -5.4]
        self.assertEqual(polynom.polySum(poly1, poly2), [0, -1.0])


    def testMultiplyEasy(self):
        poly1 = [1, 2, 5]
        poly2 = [2, 0, 1, -7]
        self.assertEqual(polynom.polyMultiply(poly1, poly2), [2, 4, 11, -5, -9, -35])


    def testMultiplyBRUTE(self):
        poly1 = [1, 2.5, 3.5, 0, 5.4]
        poly2 = [0, 12, 3.2, 3.2, 1]
        self.assertEqual(polynom.polyMultiply(poly1, poly2), [0, 12.0, 33.2, 53.2, 20.200000000000003, 78.50000000000001, 20.78, 17.28, 5.4])



if __name__ == "__main__":
    unittest.main()
