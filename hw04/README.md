# PI
## Gregoryho-Leibnizova řada
Vytvořte funkci s názvem leibnizPi s jedním parametrem (určujícím počet členů), jež sečte daný počet členů Gregoryho-Leibnizovy řady:
![alt text](./markdown-files/liebniz.png)

## Nilakantova řada
Vytvořte obdobnou funkci nilakanthaPi (také s jedním parametrem) sčítající řadu:
![alt text](./markdown-files/nilakantha.png)

Tzn. volání funkce `nilakanthaPi(1)` vrátí `3.0`, `nilakanthaPi(2)` vrátí `3.166666`, atd.

## Numerické řešení rovnice
Vytvořte funkci `newtonPi` s jedním parametrem (počáteční hodnota x<sub>0</sub>), která bude numericky řešit rovnici sin(x) = 0
 pomocí metody tečen (viz. Newtonova metoda a její použití při výpočtu odmocniny). Tento algoritmus se obecně snaží najít takové x, pro které platí f(x)=0, pomocí cyklického vyhodnocování výrazu:

![alt text](./markdown-files/newton.png)

kde f' značí derivaci funkce f. Hledání tedy začneme s nějakou počáteční hodnotu x<sub>0</sub> (parametr funkce newtonPi). Použitím uvedeného výrazu vypočteme novou hodnotu x<sub>1</sub> (krok k = 0). Tuto nově získanou hodnotu x<sub>1</sub> následně obdobně použijeme k výpočtu x<sub>2</sub> (krok k=1), atd. dokud rozdíl mezi x<sub>k</sub> a x<sub>k + 1</sub>  není zanedbatelně malý, neboli x<sub>k</sub> = x<sub>k + 1</sub>
 s ohledem na přesnost použitého datového typu proměnných. Jakmile tato podmínka nastane, tak poslední takto získanou hodnotu x prohlásíme za řešení rovnice f(x)=0.
 
- V tomto případě f(x) = sin(x) a f'(x) = cos(x)'
- Pro výpočet cos(x) a sin(x) v Pythonu použijete modul math (funkce math.cos, math.sin).
- Volání funkce newtonPi(3.0) (počáteční hodnota x<sub>0</sub> = 0.3) by mělo vrátit vámi spočtenou hodnotu konstanty Pi.
- Inspirujte se obdobným příkladem ze cvičení pro výpočet odmocniny sqrt(S), kde se takto hledá řešení pro f(x) = x<sup>2</sup> - S.