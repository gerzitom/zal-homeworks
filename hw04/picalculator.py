import math

def newtonPi(init):
    curValue = init

    while 1:
        nextValue = curValue - (math.sin(curValue) / math.cos(curValue))
        if curValue == nextValue:
            return nextValue
        else:
            curValue = nextValue


def leibnizPi(iterations):
    ret = 0
    for k in range(0, iterations):
        ret += (4 * ((-1) ** k)) / (2 * k + 1)
    return ret


def nilakanthaPi(iterations):
    if iterations == 1:
        return 3
    sum = 3
    step = 0
    j = 2

    while 1:
        step += 1
        sign = (((-1) ** step) * (-1))
        sum += sign * (4 / (j * (j + 1) * (j + 2)))
        j += 2
        if step == (iterations - 1):
            return sum

print(newtonPi(2.0))
