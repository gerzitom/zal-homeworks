def sortNumbers(weights, condition):
    weightsLength = len(weights)
    for i in range(weightsLength):
        for y in range(weightsLength - i - 1):
            if condition == "ASC":
                if weights[y] > weights[y + 1]:
                    weights[y], weights[y + 1] = weights[y + 1], weights[y]
            elif condition == "DESC":
                if weights[y] < weights[y + 1]:
                    weights[y], weights[y + 1] = weights[y + 1], weights[y]

    return weights


def sortData(weights, data, condition):

    if(len(weights) != len(data)):
        raise ValueError("Invalid input data")

    weightsLength = len(weights)

    for i in range(weightsLength):
        for y in range(weightsLength - i - 1):
            if condition == "ASC":
                if weights[y] > weights[y + 1]:
                    weights[y], weights[y + 1] = weights[y + 1], weights[y]
                    data[y], data[y + 1] = data[y + 1], data[y]
            elif condition == "DESC":
                if weights[y] < weights[y + 1]:
                    weights[y], weights[y + 1] = weights[y + 1], weights[y]
                    data[y], data[y + 1] = data[y + 1], data[y]

    return data
