import unittest
import sort

class TestPolynoms(unittest.TestCase):
    def testSortNumberASC(self):
        self.assertEqual(sort.sortNumbers([4, 2, 3], 'ASC'), [2, 3, 4])

    def testSortNumberDSC(self):
        self.assertEqual(sort.sortNumbers([4, 2, 3], 'DESC'), [4, 3, 2])

    def testSortDataACS(self):
        self.assertEqual(sort.sortData([2, 5, 6], ['Ford', 'BMW', 'Audi'], 'ASC'), ['Ford', 'BMW', 'Audi'])

    def testSortDataDESC(self):
        self.assertEqual(sort.sortData([3, 2, 4], ['Ford', 'BMW', 'Audi'], 'DESC'), ['Audi', 'Ford', 'BMW'])

    def testSortDataDifferentArrayLength(self):
        self.assertRaises(ValueError, sort.sortData, [3, 2, 2, 2], ['Ford', 'BMW', 'Audi'], "DESC")


if __name__ == "__main__":
    unittest.main()
