class Node:
    def __init__(self, value):
        self.left = None
        self.right = None
        self.value = value

    def insert(self, value):
        if self.value:
            if value < self.value:
                if self.left is None:
                    self.left = Node(value)
                else:
                    self.left.insert(value)
            else:
                if self.right is None:
                    self.right = Node(value)
                else:
                    self.right.insert(value)
        else:
            self.value = value

    def find(self, value, steps):
        if value < self.value:
            if self.left is None:
                return False
            steps += 1
            return self.left.find(value, steps)
        elif value > self.value:
            if self.right is None:
                return False
            steps += 1
            return self.right.find(value, steps)
        else:
            steps += 1
            return steps



class BinarySearchTree:
    def __init__(self):
        self.head = None
        self.visitedNodesCount = 0

    def insert(self, value):
        if self.head is None:
            self.head = Node(value)
        else:
            self.head.insert(value)

    def fromArray(self, array):
        for item in array:
            self.insert(item)

    # def search(self, value):
    #     ret = self.head.find(value, 0)
    #     if ret is not False:
    #         self.visitedNodesCount = ret
    #         return True
    #     else:
    #         self.visitedNodesCount = 0
    #         return False

    def search(self, value):
        cur = self.head
        self.visitedNodesCount = 0

        while cur.value != value:
            if value < cur.value:
                cur = cur.left
            elif value > cur.value:
                cur = cur.right
            else:
                return True

            self.visitedNodesCount += 1

            if cur is None:
                return False

        self.visitedNodesCount += 1
        return True

    def min(self):
        self.visitedNodesCount = 0
        cur = self.head
        while cur.left is not None:
            self.visitedNodesCount += 1
            cur = cur.left
        self.visitedNodesCount += 1
        return cur.value

    def max(self):
        self.visitedNodesCount = 0
        cur = self.head
        while cur.right is not None:
            self.visitedNodesCount += 1
            cur = cur.right
        self.visitedNodesCount += 1
        return cur.value

    def visitedNodes(self):
        return self.visitedNodesCount



