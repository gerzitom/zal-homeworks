import unittest
from bst import BinarySearchTree
from bst import Node

class MyTestCase(unittest.TestCase):

    def testInsert(self):
        bst = BinarySearchTree()
        bst.insert(20)
        bst.insert(40)
        bst.insert(10)
        bst.insert(24)

    def testInsertFromArray(self):
        bts = BinarySearchTree()
        bts.fromArray([5, 3, 1, 4, 7, 6, 8])

    def testSearch(self):
        bts = BinarySearchTree()
        bts.fromArray([5, 3, 1, 4, 7, 6, 8])
        print(bts.search(3))
        self.assertEqual(bts.search(3), True)
        self.assertEqual(bts.search(10), False)

    def testMin(self):
        bts = BinarySearchTree()
        bts.fromArray([5, 3, 1, 4, 7, 6, 8])
        self.assertEqual(bts.min(), 1)

    def testMax(self):
        bts = BinarySearchTree()
        bts.fromArray([5, 3, 1, 4, 7, 6, 8])
        self.assertEqual(bts.max(), 8)

    def testVisitedNodes(self):
        bts = BinarySearchTree()
        bts.fromArray([5, 3, 1, 4, 7, 6, 8])
        bts.search(5)
        self.assertEqual(bts.visitedNodes(), 1)
        bts.search(7)
        self.assertEqual(bts.visitedNodes(), 2)
        bts.search(1)
        self.assertEqual(bts.visitedNodes(), 3)

        bts.search(0)
        self.assertEqual(bts.visitedNodes(), 3)

        bts.min()
        self.assertEqual(bts.visitedNodes(), 3)

        bts.max()
        self.assertEqual(bts.visitedNodes(), 3)




if __name__ == '__main__':
    unittest.main()
