import unittest
from dijkstra import *


class MyTestCase(unittest.TestCase):
    def setUp(self):
        self.vertexes = [
            Vertex(0, 'Redville'),
            Vertex(1, 'Blueville'),
            Vertex(2, 'Greenville'),
            Vertex(3, 'Orangeville'),
            Vertex(4, 'Purpleville')
        ]
        self.edges = [
            Edge(0, 1, 5),
            Edge(0, 2, 10),
            Edge(0, 3, 8),
            Edge(1, 0, 5),
            Edge(1, 2, 3),
            Edge(1, 4, 7),
            Edge(2, 0, 10),
            Edge(2, 1, 3),
            Edge(3, 0, 8),
            Edge(3, 4, 2),
            Edge(4, 1, 7),
            Edge(4, 3, 2)
        ]

    def testCreateGraph(self):
        dijkstra = Dijkstra()
        dijkstra.createGraph(self.vertexes, self.edges)

    # def testComputePath(self):
    #     dijkstra = Dijkstra()
    #     dijkstra.createGraph(self.vertexes, self.edges)
    #
    #     for vertexToCompute in self.vertexes:
    #         dijkstra.computePath(vertexToCompute.id)
    #         print('Printing min distance from vertex:' + str(vertexToCompute.name))
    #         # Print minDitance from current vertex to each other
    #         for vertex in self.vertexes:
    #             print('Min distance to:' + str(vertex.name) + ' is: ' + str(vertex.minDistance))
    #         # Reset Dijkstra between counting
    #         dijkstra.resetDijkstra()

    def testShortestPath(self):
        dijkstra = Dijkstra()
        dijkstra.createGraph(self.vertexes, self.edges)
        for vertexToCompute in self.vertexes:
            dijkstra.computePath(vertexToCompute.id)
            print('Printing min distance from vertex:' + str(vertexToCompute.name))
            # Print minDitance from current vertex to each other
            for vertex in self.vertexes:
                print('Min distance to:' + str(vertex.name) + ' is: ' + str(vertex.minDistance))
                print('Path is:', end=" ")
                # Get shortest path to target vertex
                path = dijkstra.getShortestPathTo(vertex.id)
                for vertexInPath in path:
                    print(str(vertexInPath.name), end=" ")
                print()
            # Reset Dijkstra between counting
            print()
            print()
            dijkstra.resetDijkstra()


if __name__ == '__main__':
    unittest.main()
