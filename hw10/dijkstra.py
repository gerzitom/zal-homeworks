import math
import copy

class Vertex:
    def __init__(self, id, name):
        self.id = id
        self.name = name
        self.edges = []
        self.minDistance = math.inf
        self.previousVertex = None
        self.removed = False

    def __cmp__(self, other):
        return cmp(self.minDistance, other.minDistance)

    def __gt__(self, other):
        return self.minDistance > other.minDistance

    def __lt__(self, other):
        return self.minDistance < other.minDistance


class Edge:
    def __init__(self, source, target, weight):
        self.source = source
        self.target = target
        self.weight = weight

class AvailableVertexes:

    def __init__(self, vertexes):
        # self.vertexes = copy.deepcopy(vertexes)
        self.vertexes = vertexes

    def findVertex(self, sourceId):
        for vertex in self.vertexes:
            if vertex.id == sourceId:
                return vertex
        return False

    def removeVertex(self, sourceId):
        vertex = self.findVertex(sourceId)
        vertex.removed = True

    def allVertexesRemoved(self):
        for vertex in self.vertexes:
            if vertex.removed is False:
                return False
        return True

    def getLowest(self):
        lowest = None
        for vertex in self.vertexes:
            if vertex.removed is False:
                if lowest is None:
                    lowest = vertex
                else:
                    if vertex.minDistance < lowest.minDistance:
                        lowest = vertex
        return lowest




class Dijkstra:
    def __init__(self):
        self.vertexes = []

    def computePath(self, sourceId):

        # set all nodes as available
        availableVertexes = AvailableVertexes(self.vertexes)

        cur = availableVertexes.findVertex(sourceId)
        cur.minDistance = 0
        self.sourceId = sourceId

        while not availableVertexes.allVertexesRemoved():
            for edge in cur.edges:
                # source of edge is the same: cur
                curVertex = availableVertexes.findVertex(edge.target)
                newDistance = edge.weight + cur.minDistance
                if curVertex.minDistance > newDistance:
                    curVertex.minDistance = newDistance
                    curVertex.previousVertex = cur

            availableVertexes.removeVertex(cur.id)
            cur = availableVertexes.getLowest()

        pass


    def getShortestPathTo(self, targetId):
        availableVertexes = AvailableVertexes(self.vertexes)
        target = availableVertexes.findVertex(targetId)

        path = []

        end = False
        path.append(target)
        while target.previousVertex is not None:
            path.append(target.previousVertex)
            target = target.previousVertex

        # path.append(target)

        return path[::-1]



    def createGraph(self, vertexes, edgesToVertexes):
        self.vertexes = vertexes
        for edge in edgesToVertexes:
            self.vertexes[edge.source].edges.append(edge)

    def resetDijkstra(self):
        for vertex in self.vertexes:
            vertex.removed = False
            vertex.minDistance = math.inf
            vertex.previousVertex = None

    def getVertexes(self):
        return self.vertexes
